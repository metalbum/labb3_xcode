//
//  Task.m
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "Task.h"

@interface Task ()

@end

@implementation Task

-(Task*)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self != nil) {
        self.taskTitle = [decoder decodeObjectForKey:@"taskTitle"];
        self.highPriority = [decoder decodeBoolForKey:@"highPriority"];
        self.completed = [decoder decodeBoolForKey:@"completed"];
        self.deleteThis = [decoder decodeBoolForKey:@"deleteThis"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.taskTitle forKey:@"taskTitle"];
    [coder encodeBool:self.highPriority forKey:@"highPriority"];
    [coder encodeBool:self.completed forKey:@"completed"];
}

@end
