//
//  TasksTable.h
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TasksTable : UITableViewController
@property (nonatomic) NSMutableArray *allTasks;
@property (nonatomic) int nrOfTasks;
@end
