//
//  DetailsViewController.h
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-06.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"

@interface DetailsViewController : UIViewController
@property (nonatomic) Task *theTask;
@end
