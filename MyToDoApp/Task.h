//
//  Task.h
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject <NSCoding>

@property (nonatomic) NSString *taskTitle;
@property (nonatomic) bool completed;
@property (nonatomic) bool highPriority;
@property (nonatomic) bool deleteThis;

@end
