//
//  TasksTable.m
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "TasksTable.h"
#import "AddTaskViewController.h"
#import "DetailsViewController.h"
#import "Task.h"

@interface TasksTable ()

@property NSUserDefaults *savedTasks;
@end

@implementation TasksTable

-(NSMutableArray*) allTasks {
    
    if (!_allTasks) {
        
        Task *exampleTask1 = [[Task alloc] init];
        exampleTask1.taskTitle = @"Köp mjölk";
        exampleTask1.highPriority=NO;
        exampleTask1.deleteThis=NO;
        
        Task *exampleTask2 = [[Task alloc] init];
        exampleTask2.taskTitle = @"Bär in ved till brasan";
        exampleTask2.highPriority=NO;
        exampleTask2.deleteThis=NO;
        
        Task *exampleTask3 = [[Task alloc] init];
        exampleTask3.taskTitle = @"Mata katten";
        exampleTask3.highPriority=YES;
        exampleTask3.deleteThis=NO;
        
        
        _allTasks = [@[exampleTask1, exampleTask2, exampleTask3] mutableCopy];
        
    }
    
    return _allTasks;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"firstRun"] == nil) {
        
        [defaults setObject:@"This is first run" forKey:@"firstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        _allTasks = [[NSMutableArray alloc] init];
    }
    
    [self loadData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

-(void)saveData {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    for (int i = 0; i < self.allTasks.count; i++) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.allTasks[i]];
        [defaults setObject:data forKey:[self.allTasks[i] taskTitle]];
        [defaults synchronize];
    }
}

-(void)loadData {
    
    NSArray *keys = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    for (int i = 0; i  < keys.count; i++) {
        if (![keys[i] isEqualToString:@"NSLanguages"] && ![keys[i] isEqualToString:@"AppleLanguages"] && ![keys[i] isEqualToString:@"NSInterfaceStyle"] && ![keys[i] isEqualToString:@"firstRun"]) {
            NSData *dataToRepresentTask = [defaults objectForKey:keys[i]];
            NSString *loadedTask = [NSKeyedUnarchiver unarchiveObjectWithData:dataToRepresentTask];
            [self.allTasks addObject:loadedTask];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.nrOfTasks = self.allTasks.count;
    
    for (int i = 0; i < self.allTasks.count; i++) {
        if ([self.allTasks[i] deleteThis]) {
            [self.allTasks removeObjectAtIndex:i];
            break;
        }
    }
    
    [self saveData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.allTasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.allTasks[indexPath.row] taskTitle];
    
    if ([self.allTasks[indexPath.row] highPriority]) {
        cell.backgroundColor = [UIColor redColor];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if (![self.allTasks[indexPath.row] completed]) {
        cell.textLabel.textColor = [UIColor blackColor];
    } else {
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (Klar)", [self.allTasks[indexPath.row] taskTitle]];
        
        cell.textLabel.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"goToAddTask"]) {
        AddTaskViewController *addView = [segue destinationViewController];
        addView.allTasks = self.allTasks;
    } else if ([segue.identifier isEqualToString:@"goToDetails"]) {
        
        DetailsViewController *detailsView = [segue destinationViewController];
        
        
        
        detailsView.theTask = self.allTasks[[[self.tableView indexPathForSelectedRow] row]];
    }
    
    
}


@end
