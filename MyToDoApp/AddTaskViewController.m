//
//  AddTaskViewController.m
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "AddTaskViewController.h"
#import "Task.h"

@interface AddTaskViewController ()
@property (weak, nonatomic) IBOutlet UITextField *taskNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UISwitch *prioritySwitch;

@end

@implementation AddTaskViewController

- (IBAction)addBtnPressed:(id)sender {
    
    Task *newTask = [[Task alloc] init];
    [newTask setTaskTitle:self.taskNameTextField.text];
    [newTask setHighPriority:self.prioritySwitch.isOn];
    [self.allTasks addObject:newTask];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)textFieldChanged:(id)sender {
    
    if ([self.taskNameTextField hasText]) {
        self.addBtn.enabled = YES;
    } else {
        self.addBtn.enabled = NO;
    }
}

- (IBAction)cancelBtnPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.addBtn.enabled = NO;
    self.prioritySwitch.on = NO;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
