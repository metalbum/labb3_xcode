//
//  DetailsViewController.m
//  MyToDoApp
//
//  Created by IT-Högskolan on 2015-02-06.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "DetailsViewController.h"
//#import "Task.h"

@interface DetailsViewController ()


@property (weak, nonatomic) IBOutlet UITextField *taskNameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *completedSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *prioritySwitch;
@property (weak, nonatomic) IBOutlet UIButton *updateBtn;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.taskNameTextField.text = self.theTask.taskTitle;
    self.prioritySwitch.on = self.theTask.highPriority;
    self.completedSwitch.on = self.theTask.completed;
}

- (IBAction)updateTask:(id)sender {
    self.theTask.taskTitle = self.taskNameTextField.text;
    self.theTask.highPriority = self.prioritySwitch.isOn;
    self.theTask.completed = self.completedSwitch.isOn;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)removeBtnPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Är du säker?" message:@"Vill du verkligen ta bort denna aktivitet från listan?" delegate:self cancelButtonTitle:@"Nej, avbryt" otherButtonTitles:@"Ja, ta bort", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        [self removeTask];
    }
}

-(void)removeTask {
    self.theTask.deleteThis = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:self.theTask.taskTitle];    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)textFieldChanged:(id)sender {
    
    if ([self.taskNameTextField hasText]) {
        self.updateBtn.enabled = YES;
    } else {
        self.updateBtn.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
